variable "ami_id" {
  default = "ami-033b95fb8079dc481"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "number_of_instances" {
  default = "1"
}

variable vault_address {
    type = string
    default = "https://vault.cloudzenix.online:8200"
}
variable vault_token {
    type = string
    default = ""
}
