resource "aws_instance" "Prasad_public"{

  ami = var.ami_id
  instance_type = var.instance_type
  tags = {
      Name = "Prasad_public"
  }

user_data = <<EOF
                 #!/bin/bash
                 sudo yum install httpd -y
                 sudo systemctl start httpd
                 sudo systemctl enable httpd
                 sudo  echo "<h1>Welcome Cloudzenix</h1>" > /var/www/html/index.html
       EOF
}

