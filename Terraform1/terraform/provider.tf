
terraform {
  required_version = "> 0.12.0"
}

provider vault {
    address = var.vault_address
    token = var.vault_token
}

data "vault_aws_access_credentials" "aws_creds" {
    backend = "aws"
    role = "dev-role"
}

provider "aws" {
  region     = "us-east-1"
  access_key = "${data.vault_aws_access_credentials.aws_creds.access_key}"
  secret_key = "${data.vault_aws_access_credentials.aws_creds.secret_key}"

}
