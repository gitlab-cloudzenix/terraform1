terraform {
  backend "s3" {
    bucket = "gitlab786"
    key    = "state.tf"
    region = "us-east-1"
  }
}
